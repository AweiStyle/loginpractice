package com.awei.googlesearch.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class Main4Activity extends AppCompatActivity {

    Data[] data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);


        data = new Data[]{
                new Data("", "http://www.han-hsien.com.tw/img/hhih/slide_sm/slide_sm5.jpg"),
                new Data("", "http://www.han-hsien.com.tw/img/hhih/slide_sm/slide_sm4.jpg"),
                new Data("", "https://d2uju15hmm6f78.cloudfront.net/image/2017/03/22/6194/2018/04/04/trim_152282707927129200_1350x900.jpg"),
                new Data("", "http://2.bp.blogspot.com/-To72WuvVuGo/TV-qGZi3JCI/AAAAAAAAAJU/PpZIUaEFky0/s1600/DSC04783.JPG"),
                new Data("", "https://s-ec.bstatic.com/images/hotel/max1024x768/934/93489556.jpg"),
                new Data("", "http://www.han-hsien.com.tw/images/hp/banquet/banquet_20160805_0308595.jpg"),
        };

        LinearLayout linearLayout_1 = findViewById(R.id.linearLayout_1);

        for (int i = 0; i < linearLayout_1.getChildCount(); i++) {

            final int j = i;
            linearLayout_1.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent();
                    intent.setClass(Main4Activity.this, Main3Activity.class);
                    String dataUrl = data[j].getUrl();
                    intent.putExtra("data", dataUrl);
                    startActivity(intent);
                }

            });
        }
    }
}
