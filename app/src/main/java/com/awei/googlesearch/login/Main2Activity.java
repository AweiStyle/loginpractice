package com.awei.googlesearch.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class Main2Activity extends AppCompatActivity {

    Data[] data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);



        data = new Data[] {
                new Data("","http://www.han-hsien.com.tw/hhih/aboutus.php"),
                new Data("","http://www.han-hsien.com.tw/hhih/room.php"),
                new Data("","http://www.han-hsien.com.tw/hhih/dining.php"),
                new Data("","http://www.han-hsien.com.tw/hhih/banquet.php"),
                new Data("",""),
                new Data("","http://www.han-hsien.com.tw/hhih/spots.php"),
                new Data("","http://www.han-hsien.com.tw/hhih/newsdetail.php?type=room"),
                new Data("","http://www.han-hsien.com.tw/hhih/contact.php"),
                new Data("","https://hotels.qrgo.com.tw/han-hsien/")
        };


        initView();
    }

    private void initView() {

        LinearLayout linearLayout1 = findViewById(R.id.linearLayout1);

        for (int i = 0; i < linearLayout1.getChildCount(); i++) {

            final int j = i;
            linearLayout1.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent();
                    intent.setClass(Main2Activity.this, Main3Activity.class);
                    String dataUrl = data[j].getUrl();
                    intent.putExtra("data", dataUrl);
                    startActivity(intent);
                }

            });

        }

        LinearLayout linearLayout2 = findViewById(R.id.linearLayout2);



        for (int i = 0; i < linearLayout1.getChildCount(); i++) {


            final int finalI = i;
            linearLayout2.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent();

                        if (finalI != 1) {
                            intent.setClass(Main2Activity.this, Main3Activity.class);
                            String dataUrl = data[finalI+3].getUrl();
                            intent.putExtra("data", dataUrl);
                            startActivity(intent);
                        }else
                        {
                            intent.setClass(Main2Activity.this, Main4Activity.class);
                            startActivity(intent);
                        }
                    }

                });


        }

        LinearLayout linearLayout3 = findViewById(R.id.linearLayout3);

        for (int i = 0; i < linearLayout1.getChildCount(); i++) {

            final int j = i;
            linearLayout3.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent();
                    intent.setClass(Main2Activity.this, Main3Activity.class);
                    String dataUrl = data[j+6].getUrl();
                    intent.putExtra("data", dataUrl);
                    startActivity(intent);
                }

            });


        }
    }


}


