package com.awei.googlesearch.login;

public class Data {

    private String picUrl;
    private String url;

    public Data(String pickUrl, String url) {
        this.picUrl = pickUrl;
        this.url = url;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public String getUrl() {
        return url;
    }
}